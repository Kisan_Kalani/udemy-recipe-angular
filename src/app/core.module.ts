import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { AuthGuard } from "./auth/auth.guard";
import { AuthService } from "./auth/auth.service";
import { UserInterceptor } from "./auth/user-interceptor.service";
import { RecipeService } from "./services/recipe.service";
import { ShoppingListService } from "./services/shopping-list.service";


@NgModule({
  providers:
    [ShoppingListService,
      RecipeService,
      AuthService,
      AuthGuard,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: UserInterceptor,
        multi: true
      }]

})
export class CoreModule {

}
