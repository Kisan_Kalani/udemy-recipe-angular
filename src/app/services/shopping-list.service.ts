import { Subject } from "rxjs";
import { Ingredient } from "../common/ingredient.model";

export class ShoppingListService {

  startEditing = new Subject<number>();

  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 1),
    new Ingredient('Cake', 5)
  ];

  getIngredients() {
    return this.ingredients;
  }
  getIngByIndex(index: number) {
    return this.ingredients[index];
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient)

  }

  deleteIngredient(index: number) {
    this.ingredients.splice(index, 1)
  }

  addIngredientToShopping(ingredient: Ingredient[]) {
    this.ingredients.push(...ingredient)
    //spread func (...) it changes an array [] into list and addes it to push func
  }

  updateIngredient(index: number, newIngredient: Ingredient) {
    this.ingredients[index] = newIngredient;

  }

}
