import { EventEmitter, Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Ingredient } from "../common/ingredient.model";
import { Recipe } from "../recipes/recipe.model";
import { ShoppingListService } from "./shopping-list.service";

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>()
  // recipeSelected = new EventEmitter<Recipe>();
  recipeSelected = new Subject<Recipe>();
  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'Dhokla',
  //     'Gujarati Dish',
  //     'https://cdn2.foodviva.com/static-content/food-images/snacks-recipes/khaman-dhokla-recipe/khaman-dhokla-recipe.jpg',
  //     [
  //       new Ingredient('Maida', 10),
  //       new Ingredient('Salt', 1)
  //     ]
  //   ),
  //   new Recipe(
  //     'Panner Tikka',
  //     'Punjabi Tadka',
  //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvqIwfnO1VO0BFCkbuaAl4ejYieaONcc9-eg&usqp=CAU',
  //     [
  //       new Ingredient('Panner', 200),
  //       new Ingredient('Milk', 10)
  //     ]
  //   )
  // ]
  private recipes: Recipe[] = [];
  constructor(private slService: ShoppingListService) {

  }


  setRecipes(recip: Recipe[]) {
    this.recipes = recip;
    this.recipesChanged.next(this.recipes.slice())
  }
  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    console.log(this.recipes)
    return this.recipes[index];
  }

  addRecipe(newRecipe: Recipe) {
    this.recipes.push(newRecipe)
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe
    this.recipesChanged.next(this.recipes.slice())
  }

  addIngToShList(ingredients: Ingredient[]) {
    this.slService.addIngredientToShopping(ingredients);
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1)
    this.recipesChanged.next(this.recipes.slice())
  }

}
