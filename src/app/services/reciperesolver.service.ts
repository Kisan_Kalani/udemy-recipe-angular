import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { Recipe } from "../recipes/recipe.model";
import { DataSourceService } from '../common/datasource.service'
import { RecipeService } from "./recipe.service";

@Injectable({ providedIn: 'root' })
export class RecipeResolverService implements Resolve<Recipe[]>{

  constructor(private dataSource: DataSourceService, private recipServ: RecipeService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Recipe[] | Observable<Recipe[]> | Promise<Recipe[]> {
    const recipes = this.recipServ.getRecipes();
    if (recipes.length === 0) {
      return this.dataSource.onFetchData();
    } else {
      return recipes;
    }


  }
}
