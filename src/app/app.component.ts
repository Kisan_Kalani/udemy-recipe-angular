import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentMenu: string;

  constructor(private authserv: AuthService) {

  }
  ngOnInit(): void {
    this.authserv.keepLoggedIn();
  }
  // onMenuChange(menu: string) {
  //   this.currentMenu = menu
  // }
}
