import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { exhaustMap, map, take, tap } from "rxjs/operators";
import { AuthService } from "../auth/auth.service";
import { Recipe } from "../recipes/recipe.model";
import { RecipeService } from "../services/recipe.service";

@Injectable({ providedIn: 'root' })
export class DataSourceService {
  baseUrl: string = 'https://recipe-89988-default-rtdb.firebaseio.com/' + 'recipes.json'
  constructor(
    private http: HttpClient,
    private recipeService: RecipeService,
    private authServ: AuthService
  ) { }

  onStoreData() {
    const recipes = this.recipeService.getRecipes();
    this.http.put(this.baseUrl, recipes)
      .subscribe(response => {
        console.log(response)
      })
  }

  onFetchData() {
    // this.authServ.userSub.subscribe().unsubscribe();

    return this.authServ.userSub
      .pipe(
        take(1),//video no 302
        exhaustMap(user => this.http.get<Recipe[]>(this.baseUrl)),
        map(recipes => {
          return recipes.map(recipe => {
            return {
              ...recipe,
              ingredients: recipe.ingredients ? recipe.ingredients : []
            }
          })
        })
        , tap(recipe => {
          return this.recipeService.setRecipes(recipe)
        })
      )


    // return this.http.get<Recipe[]>(this.baseUrl)
    //   .pipe(
    //     map(recipes => {
    //       return recipes.map(recipe => {
    //         return {
    //           ...recipe,
    //           ingredients: recipe.ingredients ? recipe.ingredients : []
    //         }
    //       })
    //     }
    //     )
    //     , tap(recipe => {
    //       return this.recipeService.setRecipes(recipe)
    //     })
    //   )

  }

}
