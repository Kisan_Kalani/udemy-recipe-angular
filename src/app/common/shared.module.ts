import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { AlertComponent } from "./alert/alert.component";
import { AlertDirective } from "./alert/alert.directive";
import { DropDownDirective } from "./dropdown.directive";
import { LoadingSpinnerComponent } from "./loading-spinner.component";

@NgModule({
  declarations: [
    AlertComponent,
    DropDownDirective,
    LoadingSpinnerComponent,
    AlertDirective
  ],
  imports: [CommonModule],
  exports: [
    AlertComponent,
    DropDownDirective,
    LoadingSpinnerComponent,
    AlertDirective,
    CommonModule
  ]
})
export class SharedModule {

}
