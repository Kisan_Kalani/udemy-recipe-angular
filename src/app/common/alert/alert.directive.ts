import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: '[appAlertBox]'
})
export class AlertDirective {

  constructor(public viewcontainer: ViewContainerRef) {

  }
}
