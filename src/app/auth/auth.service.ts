import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Subject, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { UserData } from "./userData.model";
import { environment } from '../../environments/environment'
export interface AuthResponseData {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}
@Injectable()
export class AuthService {

  apiKey = environment.API_KEY;//'AIzaSyC0UWcujQS656UsmNhLrUBjs9Y0G9Rt8qg'
  signUpUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + this.apiKey
  loginUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + this.apiKey
  // userSub = new Subject<UserData>();
  userSub = new BehaviorSubject<UserData>(null);

  constructor(private http: HttpClient, private router: Router) { }

  signUp(email: string, password: string) {
    return this.http.post<AuthResponseData>(this.signUpUrl,
      {
        email: email,
        password: password,
        returnSecureToken: true
      }
    ).pipe(
      catchError(this.handleError),
      tap(data => {
        return this.handleAuthUserData(data.email, data.localId, data.idToken, +data.expiresIn)
      })
    )

  }

  login(email: string, password: string) {
    return this.http.post<AuthResponseData>(this.loginUrl,
      {
        email: email,
        password: password,
        returnSecureToken: true
      }
    ).pipe(
      catchError(this.handleError),
      tap(data => {
        return this.handleAuthUserData(data.email, data.localId, data.idToken, +data.expiresIn)
      })

    )
  }

  logout() {
    this.userSub.next(null)
    localStorage.removeItem('userData')
    this.router.navigate(['/auth'])
  }

  keepLoggedIn() {

    const userData: {
      email: string,
      localId: string,
      _token: string,
      _tokenExpDate: string
    } = JSON.parse(localStorage.getItem('userData'));

    if (userData == null) {
      return;
    }
    console.log('userData.tokenExpDate ', userData._tokenExpDate)
    const loadedUser = new UserData(userData.email, userData.localId, userData._token,
      new Date(userData._tokenExpDate))
    if (loadedUser.token) {
      this.userSub.next(loadedUser);
    }
  }

  private handleAuthUserData(email: string, localId: string, token: string, expiresIn: number) {
    const expDate = new Date(new Date().getTime() + expiresIn * 1000)
    const userData = new UserData(email, localId, token, expDate)
    this.userSub.next(userData);
    localStorage.setItem('userData', JSON.stringify(userData));
  }


  private handleError(errorResp: HttpErrorResponse) {
    let errorMessage = 'An Unexpected error happened!!'
    if (!errorResp.error.error || !errorResp.error) {
      return throwError(errorMessage);
    }
    switch (errorResp.error.error.message) {
      case 'EMAIL_EXISTS':
        errorMessage = 'Email Already Exists!';
        break;
    }
    return throwError(errorMessage);
  }
}

