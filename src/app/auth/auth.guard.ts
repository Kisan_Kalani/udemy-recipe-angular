import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuard implements CanActivate {


  constructor(private authServ: AuthService, private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> {

    //navigate using URLTree router
    return this.authServ.userSub
      .pipe(map(user => {
        const isAuth = !!user;
        if (isAuth) {
          return true;
        }
        return this.router.createUrlTree(['/auth'])
      })
      )


    //navigate using tap operator
    // return this.authServ.userSub
    //   .pipe(map(user => {
    //     return !!user;
    //   }), tap(isAuth => {
    //     if (!isAuth) {
    //       this.router.navigate(['/auth'])
    //     }
    //   })
    //   )
  }

}
