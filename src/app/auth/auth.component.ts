import { Component, ComponentFactoryResolver, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { AlertComponent } from "../common/alert/alert.component";
import { AlertDirective } from "../common/alert/alert.directive";
import { AuthResponseData, AuthService } from "./auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {

  isLoggedIn: boolean = false;
  authForm: FormGroup
  errMessage: any;
  isLoading = false;
  @ViewChild(AlertDirective) alertHost: AlertDirective
  constructor(private authService: AuthService, private router: Router, private compFactoryresolver: ComponentFactoryResolver) {

  }
  ngOnInit(): void {
    this.authForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
  }
  onSubmit() {
    if (!this.authForm.valid) {
      return;
    }
    const email = this.authForm.value.email;
    const password = this.authForm.value.password;

    let authObs: Observable<AuthResponseData>;
    this.isLoading = true
    if (this.isLoggedIn) {
      authObs = this.authService.login(email, password)
    }
    else {
      authObs = this.authService.signUp(email, password)
    }

    authObs.subscribe(resp => {
      this.isLoading = false;
      //this.errMessage = null

      this.router.navigate(['/recipes']);
      console.log(resp)
    }, errorMessage => {
      this.errMessage = errorMessage
      this.showErrorAlert(errorMessage)
      this.isLoading = false
    })
    this.authForm.reset()
  }
  onSwitchMode() {
    this.isLoggedIn = !this.isLoggedIn
  }

  onAlertClose() {
    this.errMessage = null;
  }
  private showErrorAlert(message: string) {
    const alertCompFact = this.compFactoryresolver.resolveComponentFactory(AlertComponent);

    const hostViewConRef = this.alertHost.viewcontainer
    hostViewConRef.clear()
    const viewContRef = hostViewConRef.createComponent(alertCompFact)
    viewContRef.instance.message = message;
    viewContRef.instance.close.subscribe(x => {
      hostViewConRef.clear()
    });
  }
}
