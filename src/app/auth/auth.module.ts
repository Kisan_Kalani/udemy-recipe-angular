import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../common/shared.module";
import { AuthComponent } from "./auth.component";

const routes: Routes = [
  { path: 'auth', component: AuthComponent }
]
@NgModule({
  declarations: [AuthComponent],
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule
  ],
  exports: [RouterModule, AuthComponent]
})
export class AuthModule {

}
