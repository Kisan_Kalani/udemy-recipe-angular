

export class UserData {

  constructor(public email, public localId, private _token, private _tokenExpDate) {
  }

  get token() {
    if (!this._tokenExpDate || new Date() > this._tokenExpDate) {
      return null;
    }
    return this._token;
  }

}
