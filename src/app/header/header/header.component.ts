import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { DataSourceService } from '../../common/datasource.service'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  // @Output() featureSelected = new EventEmitter<string>();
  // onSelectNav(feature: string) {
  //   this.featureSelected.emit(feature);
  // }
  isAuthenticated: boolean = false;
  constructor(private dataSourceService: DataSourceService, private authServ: AuthService) {
  }
  ngOnInit(): void {
    this.authServ.userSub.subscribe(user => {
      this.isAuthenticated = !!user
    })
  }
  onSaveData() {
    this.dataSourceService.onStoreData();
  }
  onLogout() {
    this.authServ.logout();
  }
  onGetData() {
    this.dataSourceService.onFetchData().subscribe();
  }
}
