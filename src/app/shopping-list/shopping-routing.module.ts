import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ShoppingListComponent } from "./shopping-list.component";


const routes: Routes = [
  // { path: 'shopping-list', component: ShoppingListComponent } //without lazyloading
  { path: '', component: ShoppingListComponent }  //for lazy loading
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShoppingRoutingModule {

}
