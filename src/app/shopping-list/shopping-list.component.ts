import { Component, Input, OnInit } from '@angular/core';
import { Ingredient } from '../common/ingredient.model';
import { ShoppingListService } from '../services/shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

  constructor(private sLService: ShoppingListService) {

  }
  ngOnInit(): void {
    this.ingredients = this.sLService.getIngredients()
  }
  ingredients: Ingredient[];
  addIng(ing: Ingredient) {
    this.ingredients.push(ing)

  }

  onedit(index: number) {
    this.sLService.startEditing.next(index);
  }
}
