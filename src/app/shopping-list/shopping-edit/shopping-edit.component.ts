import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Ingredient } from 'src/app/common/ingredient.model';
import { ShoppingListService } from 'src/app/services/shopping-list.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  // @ViewChild('nameInput') name: ElementRef
  // @ViewChild('amountInput') amount: ElementRef
  @ViewChild('f') slForm: NgForm
  ingredientsList = []
  isEditMode: boolean = false;
  shoppingEditIndex: number;
  EditIngItem: Ingredient;

  constructor(private slService: ShoppingListService) {

  }

  ngOnInit(): void {
    this.slService.startEditing.subscribe(
      (index: number) => {
        this.shoppingEditIndex = index;
        this.isEditMode = true;
        this.EditIngItem = this.slService.getIngByIndex(index);
        this.slForm.setValue({
          name: this.EditIngItem.name,
          amount: this.EditIngItem.amount
        })
      })

  }
  onClick(form: NgForm) {
    // const ingName = this.name.nativeElement.value
    // const ingAmount = +this.amount.nativeElement.value
    const value = form.value;
    if (this.isEditMode) {
      this.slService.updateIngredient(this.shoppingEditIndex, new Ingredient(value.name, value.amount))

    } else {
      this.slService.addIngredient(new Ingredient(value.name, value.amount))

    }
    this.isEditMode = false;
    this.slForm.reset();

  }

  onClear() {
    this.slForm.reset();
    this.isEditMode = false;
  }

  onDelete() {
    this.slService.deleteIngredient(this.shoppingEditIndex);
    this.onClear();
  }
}
