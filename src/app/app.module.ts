import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './common/shared.module';
import { CoreModule } from './core.module';
import { AuthModule } from './auth/auth.module';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    // AuthComponent,
    // LoadingSpinnerComponent,
    // AlertComponent,
    // AlertDirective,
    // DropDownDirective

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    // RecipesModule,  //commented for lazy loading
    // ShoppingModule, //commented for lazy loading
    AuthModule,
    SharedModule,
    CoreModule
  ],
  // providers: [ShoppingListService, RecipeService, AuthService, AuthGuard,
  //   {
  //     provide: HTTP_INTERCEPTORS,
  //     useClass: UserInterceptor,
  //     multi: true
  //   }]
  bootstrap: [AppComponent]
})
export class AppModule { }
