import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Ingredient } from 'src/app/common/ingredient.model';
import { RecipeService } from 'src/app/services/recipe.service';
import { Recipe } from '../../recipe.model';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  // @Input('itemDet') item: Recipe
  item: Recipe
  id: number

  constructor(private recipeService: RecipeService, private route: ActivatedRoute, private router: Router) {

  }
  ngOnInit(): void {

    this.route.params.subscribe((param: Params) => {
      this.id = +param['id']
      this.item = this.recipeService.getRecipe(+param['id']);

    })
  }

  onAddToShopList(ingredients: Ingredient[]) {
    this.recipeService.addIngToShList(ingredients)
  }
  onDelete() {
    this.recipeService.deleteRecipe(this.id)
    this.router.navigate(['../'], { relativeTo: this.route })
  }

}
