import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { DropDownDirective } from "../common/dropdown.directive";
import { SharedModule } from "../common/shared.module";
import { RecipeStartComponent } from "../recipe-start/recipe-start.component";
import { RecipeDetailComponent } from "./recipe-detail/recipe-detail/recipe-detail.component";
import { RecipeEditComponent } from "./recipe-edit/recipe-edit.component";
import { RecipeItemComponent } from "./recipe-item/recipe-item/recipe-item.component";
import { RecipeListComponent } from "./recipe-list/recipe-list/recipe-list.component";
import { RecipesRoutingModule } from "./recipes-routing.module";
import { RecipesComponent } from "./recipes.component";


@NgModule({
  declarations: [
    RecipeListComponent,
    RecipeItemComponent,
    RecipeDetailComponent,
    RecipesComponent,
    RecipeStartComponent,
    RecipeEditComponent


  ],
  imports: [
    RouterModule,
    // CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RecipesRoutingModule
  ]
})
export class RecipesModule {

}
